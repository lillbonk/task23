-- Select all the countries where the average population is more than 1 000 000 per region
SELECT name, AVG(population), region FROM country GROUP BY region, name HAVING avg(population) > 1000000