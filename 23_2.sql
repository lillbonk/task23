-- Select all the cities with the City Population and the country name where the city is situated ordered by the name of the country
select city.name AS CityName, city.population AS CityPopulation, country.name AS CountryName from city join country on city.CountryCode = country.Code order by country.name
