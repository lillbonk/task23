-- Select all the languages per country, All the languages should be comma separated in 1 column and country names should only appear once (No duplicate country names)


select country.name, group_concat(`Language` separator ',') AS Result
from country join countrylanguage on country.Code = countrylanguage.CountryCode group by country.name

